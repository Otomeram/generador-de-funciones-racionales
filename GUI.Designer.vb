<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class GUI
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblRationalFunctionNumerator = New System.Windows.Forms.Label()
        Me.lblVinculum = New System.Windows.Forms.Label()
        Me.lblRationalFunctionDenominator = New System.Windows.Forms.Label()
        Me.btnGenerate = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lblRationalFunctionNumerator
        '
        Me.lblRationalFunctionNumerator.AutoSize = True
        Me.lblRationalFunctionNumerator.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRationalFunctionNumerator.Location = New System.Drawing.Point(160, 56)
        Me.lblRationalFunctionNumerator.Name = "lblRationalFunctionNumerator"
        Me.lblRationalFunctionNumerator.Size = New System.Drawing.Size(0, 42)
        Me.lblRationalFunctionNumerator.TabIndex = 0
        '
        'lblVinculum
        '
        Me.lblVinculum.AutoSize = True
        Me.lblVinculum.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVinculum.Location = New System.Drawing.Point(144, 70)
        Me.lblVinculum.Name = "lblVinculum"
        Me.lblVinculum.Size = New System.Drawing.Size(158, 42)
        Me.lblVinculum.TabIndex = 1
        Me.lblVinculum.Text = "_______"
        '
        'lblRationalFunctionDenominator
        '
        Me.lblRationalFunctionDenominator.AutoSize = True
        Me.lblRationalFunctionDenominator.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRationalFunctionDenominator.Location = New System.Drawing.Point(160, 112)
        Me.lblRationalFunctionDenominator.Name = "lblRationalFunctionDenominator"
        Me.lblRationalFunctionDenominator.Size = New System.Drawing.Size(0, 42)
        Me.lblRationalFunctionDenominator.TabIndex = 2
        '
        'btnGenerate
        '
        Me.btnGenerate.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGenerate.Location = New System.Drawing.Point(151, 184)
        Me.btnGenerate.Name = "btnGenerate"
        Me.btnGenerate.Size = New System.Drawing.Size(121, 40)
        Me.btnGenerate.TabIndex = 3
        Me.btnGenerate.Text = "Generar"
        Me.btnGenerate.UseVisualStyleBackColor = True
        '
        'GUI
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(436, 262)
        Me.Controls.Add(Me.btnGenerate)
        Me.Controls.Add(Me.lblRationalFunctionDenominator)
        Me.Controls.Add(Me.lblRationalFunctionNumerator)
        Me.Controls.Add(Me.lblVinculum)
        Me.Name = "GUI"
        Me.Text = "Generador"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblRationalFunctionNumerator As System.Windows.Forms.Label
    Friend WithEvents lblVinculum As System.Windows.Forms.Label
    Friend WithEvents lblRationalFunctionDenominator As System.Windows.Forms.Label
    Friend WithEvents btnGenerate As System.Windows.Forms.Button

End Class
