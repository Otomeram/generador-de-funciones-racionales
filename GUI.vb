Public Class GUI
    Dim rnd As New Random()
    Dim a, b, c, d, sign As Integer

    Private Sub btnGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerate.Click
        a = rnd.Next(16)
        b = rnd.Next(16)
        c = rnd.Next(16)
        d = rnd.Next(16)
        sign = rnd.NextDouble()
        If sign > 0.5 Then
            lblRationalFunctionNumerator.Text = a & "x + " & b
        Else
            lblRationalFunctionNumerator.Text = a & "x - " & b
        End If
        sign = rnd.NextDouble()
        If sign > 0.5 Then
            lblRationalFunctionDenominator.Text = c & "x + " & d
        Else
            lblRationalFunctionDenominator.Text = c & "x - " & d
        End If

    End Sub
End Class
